// Copyright (c) 2021 Simform Solutions. All rights reserved.
// Use of this source code is governed by a MIT-style license
// that can be found in the LICENSE file.

import 'dart:math';
import 'dart:ui';

class Constants {
  Constants._();

  static final Random _random = Random();
  static const int _maxColor = 256;

  static const int hoursADay = 24;
  static const int minutesADay = 1440;

  static final List<String> weekTitles = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];

  static Color get randomColor {
    return Color.fromRGBO(
      _random.nextInt(_maxColor),
      _random.nextInt(_maxColor),
      _random.nextInt(_maxColor),
      1,
    );
  }
}
