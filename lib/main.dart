import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:school_hub/src/app.dart';
import 'package:school_hub/src/providers.dart';
import 'package:school_hub/src/settings/settings_controller.dart';
import 'package:school_hub/src/settings/settings_service.dart';
import 'package:school_hub/src/splash.dart';

void main() async {
  runApp(const SplashView());

  // Set up the SettingsController, which will glue user settings to multiple
  // Flutter Widgets.
  final settingsController = SettingsController(SettingsService());

  // Load the user's preferred theme while the splash screen is displayed.
  // This prevents a sudden theme change when the app is first displayed.
  await settingsController.loadSettings();

  // Run the app and pass in the SettingsController. The app listens to the
  // SettingsController for changes, then passes it further down to the
  // SettingsView.
  runApp(
    ProviderScope(
      overrides: [
        settingsProvider.overrideWith((ref) => settingsController),
      ],
      child: MyApp(settingsController: settingsController),
    ),
  );
}
