import 'package:flutter/material.dart';

class LogoIcon extends StatelessWidget {
  const LogoIcon({super.key});

  @override
  Widget build(BuildContext context) {
    return Icon(
      Icons.school_outlined,
      color: Theme.of(context).colorScheme.primary,
    );
  }
}
