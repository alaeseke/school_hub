import 'package:calendar_view/calendar_view.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:school_hub/src/localization/app_localizations.dart';
import 'package:school_hub/src/main/user_button.dart';
import 'package:school_hub/src/model/event.dart';
import 'package:school_hub/src/model/subtitution.dart';
import 'package:school_hub/src/providers.dart';
import 'package:school_hub/src/services/proxies/event.dart';

final eventsProvider = FutureProvider(
  (ref) async {
    final calendars = await Future.wait(
      ref
          .watch(serviceManagerProvider)
          .getAll<EventServiceProxy>()
          .map((e) => e.getEvents()),
    );
    return calendars.flattened;
  },
);

class CalendarPage extends ConsumerStatefulWidget {
  const CalendarPage({super.key});

  @override
  ConsumerState<CalendarPage> createState() => _CalendarPageState();
}

enum CalendarPageView {
  day,
  month,
}

class _CalendarPageState extends ConsumerState<CalendarPage> {
  CalendarPageView view = CalendarPageView.day;

  @override
  Widget build(BuildContext context) {
    final isCompact = MediaQuery.sizeOf(context).width < 600;
    return NestedScrollView(
      headerSliverBuilder: (context, _) => [
        SliverAppBar.large(
          automaticallyImplyLeading: isCompact,
          title: Text(AppLocalizations.of(context)!.calendarTab),
          actions: [
            IconButton(
              onPressed: () => ref.invalidate(eventsProvider),
              icon: const Icon(Icons.refresh),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  final i = (view.index + 1) % CalendarPageView.values.length;
                  view = CalendarPageView.values[i];
                });
              },
              icon: switch (view) {
                CalendarPageView.day => const Icon(Icons.calendar_view_day),
                CalendarPageView.month => const Icon(Icons.calendar_view_month),
              },
            ),
            if (isCompact) UserButton(),
          ],
        ),
      ],
      body: ref.watch(eventsProvider).map(
            data: (data) {
              final items = data.value;

              // if (items == null) {
              //   return Text('Please sign in');
              // }

              final controller = EventController<Event>();

              for (final item in items) {
                controller.add(
                  CalendarEventData(
                    title: item.title, // item.absentTeacher ?? "",
                    startTime: item.startTime,
                    endTime: item.endTime,
                    date: item.startTime,
                    event: item,
                  ),
                );
              }

              switch (view) {
                case CalendarPageView.day:
                  return DayView(
                    controller: controller,
                    heightPerMinute: 1.5,
                    // startHour: 7,
                    // endHour: 16,
                    eventTileBuilder: (date, events, boundry, start, end) {
                      final event = events.first.event!;
                      final icon = getIconForType(context, event);
                      return Card(
                        child: DefaultTextStyle.merge(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Row(
                                  children: [
                                    if (icon != null) ...[
                                      icon,
                                      const SizedBox(width: 8),
                                    ],
                                    Text(
                                      event.title,
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleSmall,
                                    ),
                                  ],
                                ),
                                // const SizedBox(height: 8),
                                // if (substitution.type ==
                                //     SubstitutionType.roomChange)
                                //   Text(
                                //     substitution.room!,
                                //     style:
                                //         Theme.of(context).textTheme.bodyMedium,
                                //   ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                case CalendarPageView.month:
                  return MonthView(
                    controller: controller,
                    cellAspectRatio: 2.5 / 1,
                  );
              }
            },
            loading: (_) => const Center(child: CircularProgressIndicator()),
            error: (error) {
              return Text('Failed to fetch substitutions:\n${error.error}');
            },
          ),
    );
  }

  String getLabel(Substitution substitution) {
    switch (substitution.type) {
      case SubstitutionType.dropped:
        return 'Dropped';
      case SubstitutionType.substitution:
        return 'Substitution';
      case SubstitutionType.substitutionWithoutTeacher:
        return 'Substitution without teacher';

      case SubstitutionType.roomChange:
        return 'Room change';
    }
  }

  Icon? getIconForType(BuildContext context, Event event) {
    switch (event.type) {
      case EventType.parentTeacherConference:
        return const Icon(Icons.people);

      case EventType.festival:
        return const Icon(Icons.party_mode);

      default:
        return null;
    }
  }

  Color getColorForType(BuildContext context, SubstitutionType type) {
    switch (type) {
      case SubstitutionType.dropped:
        return Theme.of(context).colorScheme.error;
      default:
        return Theme.of(context).colorScheme.primary;
    }
  }
}
