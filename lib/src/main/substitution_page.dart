import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:school_hub/src/localization/app_localizations.dart';
import 'package:school_hub/src/main/user_button.dart';
import 'package:school_hub/src/model/subtitution.dart';
import 'package:school_hub/src/providers.dart';
import 'package:school_hub/src/services/proxies/substitution.dart';

final substitutionsProvider = FutureProvider(
  (ref) async => ref
      .watch(serviceManagerProvider)
      .get<SubstitutionServiceProxy>()
      ?.getSubstitutions(),
  dependencies: [serviceManagerProvider],
);

class SubtitutionPage extends ConsumerStatefulWidget {
  const SubtitutionPage({super.key});

  @override
  ConsumerState<SubtitutionPage> createState() => _SubtitutionPageState();
}

class _SubtitutionPageState extends ConsumerState<SubtitutionPage> {
  @override
  Widget build(BuildContext context) {
    final isCompact = MediaQuery.sizeOf(context).width < 600;

    return NestedScrollView(
      headerSliverBuilder: (context, _) => [
        SliverAppBar.large(
          automaticallyImplyLeading: isCompact,
          title: Text(AppLocalizations.of(context)!.substitutionsTab),
          actions: [if (isCompact) UserButton()],
        ),
      ],
      // SliverToBoxAdapter(
      //   child: Wrap(
      //     spacing: 8.0,
      //     runSpacing: 8.0,
      //     children: [
      //       FilterChip(
      //         label: const Text('Today'),
      //         selected: true,
      //         showCheckmark: false,
      //         onSelected: (bool value) {},
      //       ),
      //       FilterChip(
      //         label: const Text('Tomorrow'),
      //         showCheckmark: false,
      //         onSelected: (bool value) {},
      //       ),
      //       FilterChip(
      //         label: const Text('This Week'),
      //         showCheckmark: false,
      //         onSelected: (bool value) {},
      //       ),
      //     ],
      //   ),
      // ),
      body: ref.watch(substitutionsProvider).map(
            data: (data) {
              final items = data.value;

              if (items == null) {
                return Text(AppLocalizations.of(context)!.signInRequired);
              }

              if (items.isEmpty) {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(Icons.beach_access, size: 64),
                      const SizedBox(height: 8),
                      Text(AppLocalizations.of(context)!.noEntries),
                    ],
                  ),
                );
              }

              return ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, i) {
                  return buildSubstitutionItem(
                    context,
                    items[i],
                  );
                },
                shrinkWrap: true,
              );
            },
            loading: (_) => const Center(child: CircularProgressIndicator()),
            error: (error) {
              return Text('Failed to fetch substitutions:\n${error.error}');
            },
          ),
    );
  }

  Card buildSubstitutionItem(BuildContext context, Substitution substitution) {
    return Card(
      margin: const EdgeInsets.only(bottom: 8.0),
      color: Theme.of(context).colorScheme.surfaceVariant,
      child: ListTile(
        leading: DecoratedBox(
          decoration: BoxDecoration(
            color: getColorForType(context, substitution.type),
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: getIconForType(
              context,
              substitution.type,
            ),
          ),
        ),
        title: Text(
          getLabel(substitution),
          style: Theme.of(context).textTheme.titleMedium,
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            if (substitution.type != SubstitutionType.dropped &&
                substitution.room != null)
              Text(substitution.room!),
            if (substitution.absentTeacher != null)
              Row(
                children: [
                  Text(substitution.absentTeacher!),
                  if (substitution.type != SubstitutionType.dropped) ...[
                    const Icon(Icons.arrow_forward, size: 16),
                    Text(substitution.substituteTeacher!),
                  ]
                ],
              ),
          ],
        ),
      ),
    );
  }

  Color getColorForType(BuildContext context, SubstitutionType type) {
    switch (type) {
      case SubstitutionType.dropped:
        return Theme.of(context).colorScheme.error;
      default:
        return Theme.of(context).colorScheme.primary;
    }
  }

  Icon getIconForType(BuildContext context, SubstitutionType type) {
    switch (type) {
      case SubstitutionType.dropped:
        return Icon(Icons.close, color: Theme.of(context).colorScheme.onError);
      case SubstitutionType.substitution:
        return Icon(
          Icons.people,
          color: Theme.of(context).colorScheme.onPrimary,
        );
      case SubstitutionType.substitutionWithoutTeacher:
        return Icon(
          Icons.person_outline,
          color: Theme.of(context).colorScheme.onPrimary,
        );
      case SubstitutionType.roomChange:
        return Icon(
          Icons.meeting_room,
          color: Theme.of(context).colorScheme.onPrimary,
        );
    }
  }

  String getLabel(Substitution substitution) {
    switch (substitution.type) {
      case SubstitutionType.dropped:
        return 'Dropped';
      case SubstitutionType.substitution:
        return 'Substitution';
      case SubstitutionType.substitutionWithoutTeacher:
        return 'Substitution without teacher';
      case SubstitutionType.roomChange:
        return 'Room change';
    }
  }
}
