import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:html/parser.dart';
import 'package:school_hub/src/localization/app_localizations.dart';
import 'package:school_hub/src/main/user_button.dart';
import 'package:school_hub/src/model/news.dart';
import 'package:school_hub/src/providers.dart';
import 'package:school_hub/src/services/proxies/news.dart';

final newsProvider = FutureProvider(
  (ref) async {
    return ref.watch(serviceManagerProvider).get<NewsServiceProxy>()?.getNews();
  },
  dependencies: [serviceManagerProvider],
);

class NewsCard extends StatelessWidget {
  final NewsEntry entry;

  const NewsCard({
    super.key,
    required this.entry,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              entry.title,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(height: 16),
            Text(
              parse(entry.description).firstChild?.text ?? '',
              maxLines: 5,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}

class NewsPage extends ConsumerStatefulWidget {
  const NewsPage();

  @override
  ConsumerState<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends ConsumerState<NewsPage> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.sizeOf(context).width;
    final isCompact = width < 600;
    return NestedScrollView(
      headerSliverBuilder: (_, __) => [
        SliverAppBar.large(
          automaticallyImplyLeading: isCompact,
          title: Text(AppLocalizations.of(context)!.newsTab),
          actions: [if (isCompact) UserButton()],
        ),
      ],
      body: ref.watch(newsProvider).map(
            data: (data) {
              if (data.valueOrNull == null) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(AppLocalizations.of(context)!.signInRequired),
                );
              }

              return isCompact
                  ? ListView.separated(
                      padding: const EdgeInsets.all(16.0),
                      itemCount: data.requireValue!.length,
                      itemBuilder: (_, i) =>
                          NewsCard(entry: data.requireValue![i]),
                      separatorBuilder: (_, __) => const SizedBox(height: 8),
                    )
                  : AlignedGridView.count(
                      padding: const EdgeInsets.all(16.0),
                      crossAxisCount: switch (width) {
                        > 1280 => 4,
                        > 840 => 3,
                        > 600 => 2,
                        _ => 1,
                      },
                      mainAxisSpacing: 8,
                      crossAxisSpacing: 8,
                      itemCount: data.requireValue!.length,
                      itemBuilder: (_, i) =>
                          NewsCard(entry: data.requireValue![i]),
                    );
            },
            error: (error) => Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(error.error.toString()),
            ),
            loading: (_) => const Center(child: CircularProgressIndicator()),
          ),
    );
  }
}
