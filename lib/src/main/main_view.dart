import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:school_hub/src/localization/app_localizations.dart';
import 'package:school_hub/src/logo_icon.dart';
import 'package:school_hub/src/main/calendar_page.dart';
import 'package:school_hub/src/main/dashboard_page.dart';
import 'package:school_hub/src/main/news_page.dart';
import 'package:school_hub/src/main/substitution_page.dart';
import 'package:school_hub/src/main/user_button.dart';
import 'package:school_hub/src/model/exam.dart';
import 'package:school_hub/src/providers.dart';
import 'package:school_hub/src/router.dart';
import 'package:school_hub/src/services/proxies/exam.dart';

class MainView extends ConsumerStatefulWidget {
  const MainView({super.key});

  @override
  ConsumerState<MainView> createState() => _MainViewState();
}

class _MainViewState extends ConsumerState<MainView>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;

  late AnimationController expandController;
  late Animation<double> animation;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final destinations = [
    (
      icon: const Icon(Icons.dashboard_outlined),
      selectedIcon: const Icon(Icons.dashboard),
      getLabel: (AppLocalizations localizations) => localizations.dashboardTab,
    ),
    (
      icon: const Icon(Icons.calendar_today_outlined),
      selectedIcon: const Icon(Icons.calendar_today),
      getLabel: (AppLocalizations localizations) => localizations.calendarTab,
    ),
    (
      icon: const Icon(Icons.swap_horizontal_circle_outlined),
      selectedIcon: const Icon(Icons.swap_horizontal_circle),
      getLabel: (AppLocalizations localizations) =>
          localizations.substitutionsTab,
    ),
    (
      icon: const Icon(Icons.newspaper),
      selectedIcon: null,
      getLabel: (AppLocalizations localizations) => localizations.newsTab,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final isDesktop = constraints.maxWidth >= 600;
        if (isDesktop) return buildDesktop(context);
        return buildMobile(context);
      },
    );
  }

  Widget buildBody(BuildContext context) {
    return switch (_selectedIndex) {
      1 => const CalendarPage(),
      2 => const SubtitutionPage(),
      3 => const NewsPage(),
      4 => const CalendarPage(),
      _ => const DashboardPage()
    };
  }

  Widget buildDesktop(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    final navigationRailTextStyle = GoogleFonts.robotoCondensedTextTheme(
      Theme.of(context).textTheme,
    ).labelMedium;
    return Scaffold(
      key: _scaffoldKey,
      drawer: buildDrawer(context),
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Material(
            elevation: 3,
            surfaceTintColor: Theme.of(context).colorScheme.surfaceTint,
            shadowColor: Colors.transparent,
            color: Theme.of(context).colorScheme.surface,
            child: Column(
              children: [
                const SizedBox(height: 12),
                const DrawerButton(),
                const SizedBox(height: 16),
                Expanded(
                  child: NavigationRail(
                    backgroundColor: Colors.transparent,
                    groupAlignment: 0,
                    labelType: NavigationRailLabelType.all,
                    selectedLabelTextStyle: navigationRailTextStyle,
                    unselectedLabelTextStyle: navigationRailTextStyle,
                    destinations: [
                      for (final destination in destinations)
                        NavigationRailDestination(
                          icon: destination.icon,
                          selectedIcon: destination.selectedIcon,
                          label: Text(destination.getLabel(localizations)),
                        ),
                    ],
                    selectedIndex: _selectedIndex,
                    onDestinationSelected: _onChangeIndex,
                  ),
                ),
                const SizedBox(height: 16),
                IconButton(
                  onPressed: () => context.pushNamed('settings'),
                  icon: const Icon(Icons.settings),
                ),
                UserButton(),
                const SizedBox(height: 8),
              ],
            ),
          ),
          Expanded(child: buildBody(context)),
        ],
      ),
    );
  }

  Widget buildDrawer(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    return NavigationDrawer(
      selectedIndex: _selectedIndex,
      onDestinationSelected: (i) {
        _onChangeIndex(i);
        _scaffoldKey.currentState?.closeDrawer();
      },
      children: [
        ColoredBox(
          color: Theme.of(context).colorScheme.primaryContainer,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 28.0,
              vertical: 16.0,
            ),
            child: Row(
              children: [
                const LogoIcon(),
                const SizedBox(width: 12),
                Text(
                  'School Hub',
                  style: Theme.of(context).textTheme.labelLarge!.copyWith(
                        color: Theme.of(context).colorScheme.onPrimaryContainer,
                      ),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 16),
        for (final destination in destinations)
          NavigationDrawerDestination(
            icon: destination.icon,
            selectedIcon: destination.selectedIcon,
            label: Text(destination.getLabel(localizations)),
          ),
      ],
    );
  }

  Widget? buildFAB(BuildContext context) {
    return null;
    // ignore: dead_code
    switch (_selectedIndex) {
      case 2:
        return FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () async {
            final exam = await Navigator.push<Exam>(
              context,
              MaterialPageRoute(
                builder: (context) => const AddEventPage(),
              ),
            );
            if (exam != null) {
              final service =
                  ref.read(serviceManagerProvider).get<ExamServiceProxy>();
              if (service != null) {
                await service.addExam(exam);
              }
            }
          },
        );
      default:
        return null;
    }
  }

  Widget buildMobile(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    return Scaffold(
      key: _scaffoldKey,
      drawer: buildDrawer(context),
      bottomNavigationBar: NavigationBar(
        selectedIndex: _selectedIndex,
        destinations: [
          for (final destination in destinations)
            NavigationDestination(
              icon: destination.icon,
              selectedIcon: destination.selectedIcon,
              label: destination.getLabel(localizations),
            ),
        ],
        onDestinationSelected: _onChangeIndex,
      ),
      floatingActionButton: buildFAB(context),
      body: buildBody(context),
    );
  }

  @override
  void initState() {
    super.initState();
    expandController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    animation = CurvedAnimation(
      parent: expandController,
      curve: Curves.fastOutSlowIn,
    );
  }

  void _onChangeIndex(int index) {
    setState(() => _selectedIndex = index);
  }
}
