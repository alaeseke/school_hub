import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:school_hub/src/authentication/lspb_login_dialog.dart';
import 'package:school_hub/src/providers.dart';
import 'package:school_hub/src/services/lspb/client.dart';
import 'package:school_hub/src/services/manager.dart';

class UserButton extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return IconButton(
      icon: const CircleAvatar(child: Icon(Icons.account_circle)),
      onPressed: () async {
        await showDialog(
          context: context,
          builder: (context) => LspbLoginDialog(
            onLogin: (username, password) async {
              final navigator = Navigator.of(context);
              final serviceManager = ref.read(serviceManagerProvider);
              await _onLogin(serviceManager, username, password);
              navigator.pop();
            },
          ),
        );
      },
    );
  }

  Future<void> _onLogin(
    ServiceManager serviceManager,
    String username,
    String password,
  ) async {
    final client = LspbClient();
    final loginCheckResponse = await client.service.loginCheck({
      '_username': username,
      '_password': password,
    });

    if (!(loginCheckResponse.body?.success ?? false)) {
      throw Exception('Login failed');
    }

    final user = await client.service.getUser();
    if (!(user.body?.isLoggedIn ?? false)) {
      throw Exception('Login failed');
    }

    serviceManager.register(client);
  }
}
