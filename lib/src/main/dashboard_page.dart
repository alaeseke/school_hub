import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:school_hub/src/localization/app_localizations.dart';
import 'package:school_hub/src/main/calendar_page.dart';
import 'package:school_hub/src/main/user_button.dart';
import 'package:school_hub/src/model/event.dart';
import 'package:school_hub/src/providers.dart';
import 'package:school_hub/src/services/proxies/identity.dart';

final vacationProvider = FutureProvider(
  (ref) {
    final events = ref.watch(eventsProvider).valueOrNull;
    final now = DateTime.now();
    return events?.firstWhereOrNull(
      (e) =>
          e.startTime.isBefore(now) &&
          (e.endTime?.isAfter(now) ?? false) &&
          e.type == EventType.holidays,
    );
  },
  dependencies: [eventsProvider],
);

final nameProvider = FutureProvider(
  (ref) async {
    return ref
        .watch(serviceManagerProvider)
        .get<IdentityServiceProxy>()
        ?.getIdentity();
  },
  dependencies: [serviceManagerProvider],
);

class DashboardPage extends ConsumerWidget {
  const DashboardPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isCompact = MediaQuery.sizeOf(context).width < 600;
    final currentHoliday = ref.watch(vacationProvider).valueOrNull;
    final holidays = currentHoliday?.endTime?.difference(DateTime.now()).inDays;
    final name =
        ref.watch(nameProvider).valueOrNull?.split(' ').firstOrNull ?? '';
    return NestedScrollView(
      headerSliverBuilder: (context, _) => [
        SliverAppBar.large(
          automaticallyImplyLeading: isCompact,
          title: Text(
            switch (DateTime.now().hour) {
              > 18 ||
              < 3 =>
                AppLocalizations.of(context)!.greetingEvening(name),
              > 12 => AppLocalizations.of(context)!.greetingAfternoon(name),
              > 3 => AppLocalizations.of(context)!.greetingMorning(name),
              _ => AppLocalizations.of(context)!.greetingGeneric(name),
            },
          ),
          actions: [if (isCompact) UserButton()],
        ),
      ],
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            if (currentHoliday != null)
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        currentHoliday.title,
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      const SizedBox(height: 8),
                      Text(
                        holidays! > 7
                            ? AppLocalizations.of(context)!
                                .holidaysTextWeeks(holidays ~/ 7)
                            : AppLocalizations.of(context)!
                                .holidaysTextDays(holidays),
                      ),
                    ],
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
