import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:school_hub/src/services/ferien_wiki.dart';
import 'package:school_hub/src/services/local/local.dart';
import 'package:school_hub/src/services/manager.dart';
import 'package:school_hub/src/settings/settings_controller.dart';

final settingsProvider = ChangeNotifierProvider<SettingsController>(
  (_) => throw UnimplementedError(),
);

final serviceManagerProvider = ChangeNotifierProvider<ServiceManager>(
  (_) => ServiceManager()
    ..register(LocalService())
    ..register(FerienWiki()),
);
