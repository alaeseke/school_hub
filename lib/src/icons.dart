import 'package:flutter/widgets.dart';

class SchoolHubIcons {
  SchoolHubIcons._();

  static const _fontFamily = 'SchoolHub';
  static const IconData lspb = IconData(0xe800, fontFamily: _fontFamily);
}
