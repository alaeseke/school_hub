import 'app_localizations.dart';

/// The translations for German (`de`).
class AppLocalizationsDe extends AppLocalizations {
  AppLocalizationsDe([String locale = 'de']) : super(locale);

  @override
  String get appTitle => 'School Hub';

  @override
  String greetingGeneric(Object name) {
    return 'Hey $name';
  }

  @override
  String greetingMorning(Object name) {
    return 'Guten Morgen $name';
  }

  @override
  String greetingAfternoon(Object name) {
    return 'Guten Nachmittag $name';
  }

  @override
  String greetingEvening(Object name) {
    return 'Guten Abend $name';
  }

  @override
  String get usernameHint => 'Benutzername';

  @override
  String get passwordHint => 'Kennwort';

  @override
  String get signInButtonLabel => 'Anmelden';

  @override
  String get substitutionsTab => 'Vertretungen';

  @override
  String get scheduleTab => 'Stundenplan';

  @override
  String get newsTab => 'Neuigkeiten';

  @override
  String get dashboardTab => 'Übersicht';

  @override
  String get calendarTab => 'Kalendar';

  @override
  String get noEntries => 'Keine Einträge';

  @override
  String holidaysTextDays(Object days) {
    return 'Genieße deine Ferien! Schule fängt wieder in $days Tagen an.';
  }

  @override
  String holidaysTextWeeks(Object weeks) {
    return 'Genieße deine Ferien! Schule fängt wieder in $weeks Wochen an.';
  }

  @override
  String get signInRequired => 'Du musst Dich anmelden um diese Funktion nutzen zu können.';
}
