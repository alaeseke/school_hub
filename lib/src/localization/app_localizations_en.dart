import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get appTitle => 'School Hub';

  @override
  String greetingGeneric(Object name) {
    return 'Hey $name';
  }

  @override
  String greetingMorning(Object name) {
    return 'Good morning $name';
  }

  @override
  String greetingAfternoon(Object name) {
    return 'Good afternoon $name';
  }

  @override
  String greetingEvening(Object name) {
    return 'Good evening $name';
  }

  @override
  String get usernameHint => 'Username';

  @override
  String get passwordHint => 'Password';

  @override
  String get signInButtonLabel => 'Sign in';

  @override
  String get substitutionsTab => 'Substitutions';

  @override
  String get scheduleTab => 'Schedule';

  @override
  String get newsTab => 'News';

  @override
  String get dashboardTab => 'Dashboard';

  @override
  String get calendarTab => 'Calendar';

  @override
  String get noEntries => 'No Entries';

  @override
  String holidaysTextDays(Object days) {
    return 'Enjoy your vacation! School begins again in $days days from now.';
  }

  @override
  String holidaysTextWeeks(Object weeks) {
    return 'Enjoy your vacation! School begins again in $weeks weeks from now.';
  }

  @override
  String get signInRequired => 'You have to sign in to use this feature.';
}
