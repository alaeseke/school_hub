import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter/material.dart';
import 'package:school_hub/color_schemes.g.dart';
import 'package:school_hub/src/onboarding/choose_school_page.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen({super.key});

  @override
  State<OnboardingScreen> createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  bool _showSetup = false;

  @override
  Widget build(BuildContext context) {
    final primary = Theme.of(context).colorScheme.primary;
    final onPrimary = Theme.of(context).colorScheme.onPrimary;

    if (_showSetup) return buildSetup(context);

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DecoratedBox(
              decoration: BoxDecoration(
                color:
                    Theme.of(context).colorScheme.brightness == Brightness.light
                        ? lightColorScheme.primary.harmonizeWith(primary)
                        : darkColorScheme.primary.harmonizeWith(primary),
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(24.0),
                child: Icon(
                  Icons.school_outlined,
                  size: 72,
                  color: Theme.of(context).colorScheme.brightness ==
                          Brightness.light
                      ? lightColorScheme.onPrimary.harmonizeWith(onPrimary)
                      : darkColorScheme.onPrimary.harmonizeWith(onPrimary),
                ),
              ),
            ),
            const SizedBox(height: 56),
            Text(
              'School Hub',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            const SizedBox(height: 8),
            Text(
              'A place for everything school related',
              style: Theme.of(context).textTheme.titleSmall,
            ),
            const SizedBox(height: 56),
            FilledButton(
              child: const Text('Get Started'),
              onPressed: () => setState(() => _showSetup = true),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildSetup(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ChooseSchoolPage(
              onSchoolSelected: (value) {},
            ),
          ),
          const Divider(height: 1),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Directionality(
                  textDirection: TextDirection.ltr,
                  child: TextButton.icon(
                    icon: const Icon(Icons.chevron_left),
                    label: const Text('Back'),
                    onPressed: () {},
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: TextButton.icon(
                    icon: const Icon(Icons.chevron_left),
                    label: const Text('Next'),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
