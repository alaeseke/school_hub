import 'package:flutter/material.dart';

class NamePage extends StatelessWidget {
  const NamePage({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Text(
            'Tell us your name',
            style: Theme.of(context).textTheme.headlineSmall,
          ),
          const SizedBox(height: 24),
          const TextField(
            decoration: InputDecoration(
              hintText: 'Name',
              border: OutlineInputBorder(),
              helperText: 'Your name is only used to greet and refer to you 🤗',
            ),
          ),
        ],
      ),
    );
  }
}
