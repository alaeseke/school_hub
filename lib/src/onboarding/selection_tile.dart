import 'package:flutter/material.dart';

class SelectionTile extends StatelessWidget {
  final Widget title;
  final Widget description;
  final Widget? icon;
  final bool selected;
  final VoidCallback? onTap;

  const SelectionTile({
    super.key,
    required this.title,
    required this.description,
    this.icon,
    this.selected = false,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final icon = this.icon;
    final enabled = onTap != null;
    final Color borderColor;
    final iconColor =
        enabled ? theme.colorScheme.primary : theme.colorScheme.outlineVariant;

    if (!enabled) {
      borderColor = theme.colorScheme.outlineVariant;
    } else if (selected) {
      borderColor = theme.colorScheme.primary;
    } else {
      borderColor = theme.colorScheme.outline;
    }

    final textColor = enabled ? null : theme.colorScheme.outlineVariant;

    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color: borderColor, width: selected ? 2.0 : 1.0),
        borderRadius: const BorderRadius.all(Radius.circular(12)),
      ),
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (icon != null)
                IconTheme(
                  data: IconThemeData(color: iconColor),
                  child: icon,
                ),
              const SizedBox(width: 16),
              Expanded(
                child: DefaultTextStyle.merge(
                  style: TextStyle(color: textColor),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      DefaultTextStyle(
                        style: Theme.of(context)
                            .textTheme
                            .labelLarge!
                            .copyWith(color: textColor),
                        child: title,
                      ),
                      const SizedBox(height: 4),
                      description,
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
