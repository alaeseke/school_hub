import 'package:flutter/material.dart';
import 'package:school_hub/src/icons.dart';
import 'package:school_hub/src/model/school.dart';
import 'package:school_hub/src/onboarding/selection_tile.dart';

class ChooseSchoolPage extends StatefulWidget {
  final ValueChanged<School>? onSchoolSelected;

  const ChooseSchoolPage({super.key, required this.onSchoolSelected});

  @override
  State<ChooseSchoolPage> createState() => _ChooseSchoolPageState();
}

class _ChooseSchoolPageState extends State<ChooseSchoolPage> {
  ChooseSchoolSubPage subpage = ChooseSchoolSubPage.selectSchoolSource;

  @override
  Widget build(BuildContext context) {
    switch (subpage) {
      case ChooseSchoolSubPage.selectSchoolSource:
        return SingleChildScrollView(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Text(
                'Choose your school',
                style: Theme.of(context).textTheme.headlineSmall,
              ),
              const SizedBox(height: 24),
              SelectionTile(
                icon: const Icon(Icons.location_on),
                title: const Text('Search nearby schools'),
                description: const Text('Find close schools in your area'),
                onTap: () => setState(
                  () => subpage = ChooseSchoolSubPage.selectFromNearby,
                ),
              ),
              const SizedBox(height: 8),
              SelectionTile(
                icon: const Icon(Icons.search),
                title: const Text('Manually find a school'),
                description: const Text('Type in the name of your school'),
                onTap: () => setState(
                  () => subpage = ChooseSchoolSubPage.selectFromList,
                ),
              ),
            ],
          ),
        );
      case ChooseSchoolSubPage.selectFromList:
        return buildManualSelection(context);
      case ChooseSchoolSubPage.selectFromNearby:
        return buildManualSelection(context);
      case ChooseSchoolSubPage.selectServices:
        return buildSelectServices(context);
    }
  }

  Widget buildManualSelection(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 16),
        Text(
          'Choose your school',
          style: Theme.of(context).textTheme.headlineSmall,
        ),
        const SizedBox(height: 24),
        Expanded(
          child: ListView.separated(
            itemCount: 100 + 1,
            separatorBuilder: (context, index) {
              return const Divider(height: 1);
            },
            itemBuilder: (context, index) {
              if (index == 0) {
                return ListTile(
                  leading: const Icon(Icons.add),
                  title: const Text('School not listed here?'),
                  subtitle: const Text('Add services manually'),
                  onTap: () => setState(
                    () => subpage = ChooseSchoolSubPage.selectServices,
                  ),
                );
              }

              return ListTile(
                title: Text('School $index'),
                leading: const Icon(Icons.business),
                trailing: const Icon(Icons.chevron_right),
                subtitle: const Text('Willistraße 69'),
                onTap: () {},
              );
            },
          ),
        ),
      ],
    );
  }

  Widget buildSelectServices(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 16),
        Text(
          'Select services',
          style: Theme.of(context).textTheme.headlineSmall,
        ),
        const SizedBox(height: 24),
        Expanded(
          child: Column(
            children: [
              CheckboxListTile(
                title: const Text('Lernstatt-Paderborn'),
                secondary: const Icon(SchoolHubIcons.lspb),
                value: true,
                onChanged: (value) {},
              ),
              CheckboxListTile(
                title: const Text('Untis'),
                secondary: const Icon(Icons.business),
                value: true,
                onChanged: (value) {},
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildSetupServices(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 16),
        Text(
          'Setup services',
          style: Theme.of(context).textTheme.headlineSmall,
        ),
        const SizedBox(height: 24),
        Expanded(
          child: Column(
            children: [
              ListTile(
                title: const Text('Lernstatt-Paderborn'),
                leading: const Icon(SchoolHubIcons.lspb),
                subtitle: const Text('Not set up yet'),
                onTap: () {},
              ),
            ],
          ),
        ),
      ],
    );
  }
}

enum ChooseSchoolSubPage {
  selectSchoolSource,
  selectFromList,
  selectFromNearby,
  selectServices,
}
