import 'package:school_hub/src/model/event.dart';

abstract class EventServiceProxy {
  Future<List<Event>> getEvents();
}
