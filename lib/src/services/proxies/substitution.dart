import 'package:school_hub/src/model/subtitution.dart';

abstract class SubstitutionServiceProxy {
  Future<List<Substitution>> getSubstitutions();
}
