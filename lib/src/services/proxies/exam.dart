import 'package:school_hub/src/model/exam.dart';

abstract class ExamServiceProxy {
  Future<List<Exam>> getExams();

  Future<void> addExam(Exam exam);
}
