abstract class IdentityServiceProxy {
  Future<String> getIdentity();
}
