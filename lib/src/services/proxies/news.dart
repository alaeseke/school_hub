import 'package:school_hub/src/model/news.dart';

abstract class NewsServiceProxy {
  Future<List<NewsEntry>> getNews();
}
