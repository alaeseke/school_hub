import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart' show ChangeNotifier;

class ServiceManager extends ChangeNotifier {
  final List<Object> _services = [];

  T? get<T>() => _services.firstWhereOrNull((e) => e is T) as T?;

  Iterable<T> getAll<T>() => _services.whereType<T>();

  void register<T extends Object>(T service) {
    assert(_services.none((e) => e is T));
    _services.add(service);
    notifyListeners();
  }
}
