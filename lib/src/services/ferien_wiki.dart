import 'package:http/http.dart';
import 'package:icalendar_parser/icalendar_parser.dart';
import 'package:school_hub/src/model/event.dart';
import 'package:school_hub/src/services/proxies/event.dart';

class FerienWiki implements EventServiceProxy {
  @override
  Future<List<Event>> getEvents() async {
    final year = DateTime.now().year;
    final url = Uri.https('www.ferienwiki.de').replace(
      pathSegments: [
        'exports',
        'ferien',
        year.toString(),
        'de',
        'nordrhein-westfalen'
      ],
    );
    final response = await get(url);
    final calendar = ICalendar.fromString(response.body);

    return [
      for (final item in calendar.data) _toEvent(item),
    ];
  }

  Event _toEvent(Map<String, dynamic> map) {
    return Event(
      type: EventType.holidays,
      title: map['summary'] as String,
      startTime: (map['dtstart'] as IcsDateTime).toDateTime()!,
      endTime: (map['dtend'] as IcsDateTime).toDateTime(),
    );
  }
}
