import 'package:school_hub/src/model/exam.dart';
import 'package:school_hub/src/services/proxies/exam.dart';

class LocalService implements ExamServiceProxy {
  List<Exam> exams = [];

  @override
  Future<List<Exam>> getExams() async {
    return exams;
  }

  @override
  Future<void> addExam(Exam exam) async {
    exams.add(exam);
  }
}
