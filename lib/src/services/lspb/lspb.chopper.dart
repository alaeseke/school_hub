// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lspb.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations, unnecessary_brace_in_string_interps
class _$LspbService extends LspbService {
  _$LspbService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = LspbService;

  @override
  Future<Response<LoginCheckResponse>> loginCheck(Map<String, String> request) {
    final Uri $url = Uri.parse('https://start.lspb.de/login_check');
    final $body = request;
    final Request $request = Request(
      'POST',
      $url,
      client.baseUrl,
      body: $body,
    );
    return client.send<LoginCheckResponse, LoginCheckResponse>($request);
  }

  @override
  Future<Response<dynamic>> logout() {
    final Uri $url = Uri.parse('https://start.lspb.de/logout');
    final Request $request = Request(
      'POST',
      $url,
      client.baseUrl,
    );
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<List<NewsEntry>>> getNewsEntries() {
    final Uri $url =
        Uri.parse('https://start.lspb.de/news/api/v1/entries.json');
    final Request $request = Request(
      'GET',
      $url,
      client.baseUrl,
    );
    return client.send<List<NewsEntry>, NewsEntry>($request);
  }

  @override
  Future<Response<InboxResponse>> getInbox() {
    final Uri $url = Uri.parse('https://start.lspb.de/news/api/v1/inbox.json');
    final Request $request = Request(
      'GET',
      $url,
      client.baseUrl,
    );
    return client.send<InboxResponse, InboxResponse>($request);
  }

  @override
  Future<Response<UserResponse>> getUser() {
    final Uri $url = Uri.parse('https://start.lspb.de/portal/api/v1/user.json');
    final Request $request = Request(
      'GET',
      $url,
      client.baseUrl,
    );
    return client.send<UserResponse, UserResponse>($request);
  }

  @override
  Future<Response<SchedulesResponse>> getSchedules() {
    final Uri $url =
        Uri.parse('https://start.lspb.de/substitution/api/v1/schedules.json');
    final Request $request = Request(
      'GET',
      $url,
      client.baseUrl,
    );
    return client.send<SchedulesResponse, SchedulesResponse>($request);
  }
}
