import 'package:json_annotation/json_annotation.dart';

part 'mail_content.g.dart';

@JsonSerializable()
class MailContentResponse {
  final String content;

  const MailContentResponse(this.content);

  factory MailContentResponse.fromJson(Map<String, dynamic> json) =>
      _$MailContentResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MailContentResponseToJson(this);
}
