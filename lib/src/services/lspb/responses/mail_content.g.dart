// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mail_content.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MailContentResponse _$MailContentResponseFromJson(Map<String, dynamic> json) =>
    MailContentResponse(
      json['content'] as String,
    );

Map<String, dynamic> _$MailContentResponseToJson(
  MailContentResponse instance,
) =>
    <String, dynamic>{
      'content': instance.content,
    };
