// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inbox.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InboxResponse _$InboxResponseFromJson(Map<String, dynamic> json) =>
    InboxResponse(
      json['numMessages'] as int,
    );

Map<String, dynamic> _$InboxResponseToJson(InboxResponse instance) =>
    <String, dynamic>{
      'numMessages': instance.numMessages,
    };
