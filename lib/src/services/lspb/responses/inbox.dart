import 'package:json_annotation/json_annotation.dart';

part 'inbox.g.dart';

@JsonSerializable()
class InboxResponse {
  final int numMessages;

  const InboxResponse(this.numMessages);

  factory InboxResponse.fromJson(Map<String, dynamic> json) =>
      _$InboxResponseFromJson(json);

  Map<String, dynamic> toJson() => _$InboxResponseToJson(this);
}
