// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedules.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SchedulesResponse _$SchedulesResponseFromJson(Map<String, dynamic> json) =>
    SchedulesResponse(
      status: json['status'] as String,
      infoMessage: json['infoMessage'] as String,
      headers:
          (json['headers'] as List<dynamic>).map((e) => e as String).toList(),
      entries: (json['entries'] as List<dynamic>)
          .map((e) => ScheduleEntry.fromJson(e as Map<String, dynamic>))
          .toList(),
      role: $enumDecode(_$ScheduleRoleEnumMap, json['role']),
    );

Map<String, dynamic> _$SchedulesResponseToJson(SchedulesResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'infoMessage': instance.infoMessage,
      'headers': instance.headers,
      'entries': instance.entries,
      'role': _$ScheduleRoleEnumMap[instance.role]!,
    };

const _$ScheduleRoleEnumMap = {
  ScheduleRole.student: 'ROLE_USER_SCHUELER',
  ScheduleRole.teacher: 'ROLE_USER_LEHRER',
};
