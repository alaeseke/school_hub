// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_check.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginCheckResponse _$LoginCheckResponseFromJson(Map<String, dynamic> json) =>
    LoginCheckResponse(
      json['success'] as bool,
      json['message'] as String?,
    );

Map<String, dynamic> _$LoginCheckResponseToJson(LoginCheckResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
    };
