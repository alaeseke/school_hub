import 'package:json_annotation/json_annotation.dart';
import 'package:school_hub/src/services/lspb/entities/schedule_entry.dart';

part 'schedules.g.dart';

@JsonSerializable()
class SchedulesResponse {
  final String status;
  final String infoMessage;
  final List<String> headers;
  final List<ScheduleEntry> entries;
  final ScheduleRole role;

  const SchedulesResponse({
    required this.status,
    required this.infoMessage,
    required this.headers,
    required this.entries,
    required this.role,
  });

  factory SchedulesResponse.fromJson(Map<String, dynamic> json) =>
      _$SchedulesResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SchedulesResponseToJson(this);
}

@JsonEnum()
enum ScheduleRole {
  @JsonValue('ROLE_USER_SCHUELER')
  student,

  @JsonValue('ROLE_USER_LEHRER')
  teacher,
}
