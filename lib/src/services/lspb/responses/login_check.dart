import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'login_check.g.dart';

@JsonSerializable()
@immutable
class LoginCheckResponse {
  final bool success;
  final String? message;

  // ignore: avoid_positional_boolean_parameters
  const LoginCheckResponse(this.success, [this.message]);

  factory LoginCheckResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginCheckResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginCheckResponseToJson(this);
}
