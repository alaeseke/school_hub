import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class UserResponse {
  @JsonKey(name: 'displayname')
  final String? displayName;

  @JsonKey(name: 'isloggedin')
  final bool isLoggedIn;

  const UserResponse({
    this.displayName,
    required this.isLoggedIn,
  });

  factory UserResponse.fromJson(Map<String, dynamic> json) =>
      _$UserResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserResponseToJson(this);
}
