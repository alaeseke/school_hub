import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'login_check.g.dart';

@JsonSerializable()
@immutable
class LoginCheckRequest {
  @JsonKey(name: '_username')
  final String username;

  @JsonKey(name: '_password')
  final String password;

  const LoginCheckRequest(this.username, this.password);

  factory LoginCheckRequest.fromJson(Map<String, dynamic> json) =>
      _$LoginCheckRequestFromJson(json);

  Map<String, dynamic> toJson() => _$LoginCheckRequestToJson(this);
}
