// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_check.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginCheckRequest _$LoginCheckRequestFromJson(Map<String, dynamic> json) =>
    LoginCheckRequest(
      json['_username'] as String,
      json['_password'] as String,
    );

Map<String, dynamic> _$LoginCheckRequestToJson(LoginCheckRequest instance) =>
    <String, dynamic>{
      '_username': instance.username,
      '_password': instance.password,
    };
