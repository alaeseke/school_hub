import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';

// ignore: must_be_immutable
class PhpSessionInterceptor implements RequestInterceptor, ResponseInterceptor {
  String? phpSessionId;

  PhpSessionInterceptor();

  @override
  FutureOr<Request> onRequest(Request request) {
    if (phpSessionId == null) return request;

    debugPrint('Applying id: $phpSessionId');
    return request.copyWith(
      headers: {
        ...request.headers,
        'Cookie': 'PHPSESSID=$phpSessionId',
      },
    );
  }

  @override
  FutureOr<Response> onResponse(Response response) {
    final cookiesHeader = response.headers['set-cookie'];
    if (cookiesHeader == null) return response;

    final entries = cookiesHeader
        .split(',')
        .map((e) => e.split('; ')[0])
        .map((e) => e.split('='))
        .map((e) => MapEntry(e[0], e[1]))
        .toList();

    // Lernstatt sends two Set-Cookie headers.
    final phpSessionId =
        entries.lastWhereOrNull((e) => e.key == 'PHPSESSID')?.value;
    if (phpSessionId != null) this.phpSessionId = phpSessionId;

    return response;
  }
}
