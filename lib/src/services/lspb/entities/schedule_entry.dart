import 'package:json_annotation/json_annotation.dart';

part 'schedule_entry.g.dart';

@JsonSerializable()
class ScheduleEntry {
  final String? color;
  final List<String> items;

  const ScheduleEntry(this.color, this.items);

  factory ScheduleEntry.fromJson(Map<String, dynamic> json) =>
      _$ScheduleEntryFromJson(json);

  Map<String, dynamic> toJson() => _$ScheduleEntryToJson(this);
}
