// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_entry.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsEntry _$NewsEntryFromJson(Map<String, dynamic> json) => NewsEntry(
      json['title'] as String,
      Uri.parse(json['permalink'] as String),
      json['content'] as String,
    );

Map<String, dynamic> _$NewsEntryToJson(NewsEntry instance) => <String, dynamic>{
      'title': instance.title,
      'permalink': instance.permalink.toString(),
      'content': instance.content,
    };
