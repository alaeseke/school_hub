// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule_entry.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScheduleEntry _$ScheduleEntryFromJson(Map<String, dynamic> json) =>
    ScheduleEntry(
      json['color'] as String?,
      (json['items'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$ScheduleEntryToJson(ScheduleEntry instance) =>
    <String, dynamic>{
      'color': instance.color,
      'items': instance.items,
    };
