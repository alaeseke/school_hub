import 'package:json_annotation/json_annotation.dart';

part 'news_entry.g.dart';

@JsonSerializable()
class NewsEntry {
  final String title;
  final Uri permalink;
  final String content;

  const NewsEntry(this.title, this.permalink, this.content);

  factory NewsEntry.fromJson(Map<String, dynamic> json) =>
      _$NewsEntryFromJson(json);

  Map<String, dynamic> toJson() => _$NewsEntryToJson(this);
}
