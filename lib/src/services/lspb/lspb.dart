import 'package:chopper/chopper.dart';
import 'package:school_hub/src/services/lspb/entities/news_entry.dart';
import 'package:school_hub/src/services/lspb/responses/inbox.dart';
import 'package:school_hub/src/services/lspb/responses/login_check.dart';
import 'package:school_hub/src/services/lspb/responses/schedules.dart';
import 'package:school_hub/src/services/lspb/responses/user.dart';

part 'lspb.chopper.dart';

@ChopperApi(baseUrl: 'https://start.lspb.de')
abstract class LspbService extends ChopperService {
  static LspbService create([ChopperClient? client]) => _$LspbService(client);

  @Post(path: 'login_check')
  Future<Response<LoginCheckResponse>> loginCheck(
    @Body() Map<String, String> request,
  );

  @Post(path: 'logout')
  Future<Response> logout();

  @Get(path: 'news/api/v1/entries.json')
  Future<Response<List<NewsEntry>>> getNewsEntries();

  @Get(path: 'news/api/v1/inbox.json')
  Future<Response<InboxResponse>> getInbox();

  @Get(path: 'portal/api/v1/user.json')
  Future<Response<UserResponse?>> getUser();

  @Get(path: 'substitution/api/v1/schedules.json')
  Future<Response<SchedulesResponse>> getSchedules();
}
