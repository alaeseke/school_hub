import 'package:chopper/chopper.dart';
import 'package:intl/intl.dart';
import 'package:school_hub/src/json_serializable_converter.dart';
import 'package:school_hub/src/model/news.dart';
import 'package:school_hub/src/model/subtitution.dart';
import 'package:school_hub/src/services/lspb/entities/news_entry.dart' as lspb;
import 'package:school_hub/src/services/lspb/interceptors/php_session_interceptor.dart';
import 'package:school_hub/src/services/lspb/lspb.dart';
import 'package:school_hub/src/services/lspb/responses/inbox.dart';
import 'package:school_hub/src/services/lspb/responses/login_check.dart';
import 'package:school_hub/src/services/lspb/responses/schedules.dart';
import 'package:school_hub/src/services/lspb/responses/user.dart';
import 'package:school_hub/src/services/proxies/identity.dart';
import 'package:school_hub/src/services/proxies/news.dart';
import 'package:school_hub/src/services/proxies/substitution.dart';

Map<String, SubstitutionType> get _defaultSubstitutionTypeMap => {
      'Vertretung': SubstitutionType.substitution,
      'Vtr. ohne Lehrer': SubstitutionType.substitutionWithoutTeacher,
      'Statt-Vertretung': SubstitutionType.substitution,
      'Entfall': SubstitutionType.dropped,
      'Raum-Vtr.': SubstitutionType.roomChange,
    };

class LspbClient
    implements
        SubstitutionServiceProxy,
        NewsServiceProxy,
        IdentityServiceProxy {
  late final ChopperClient client;
  late final LspbService service;
  late final PhpSessionInterceptor phpSessionInterceptor;

  /// A map to map the table headers to the substitution properties.
  late Map<String, LspbSubstitutionProperty> substitutionHeaderMap;

  /// A map to map the substitution type label to [SubstitutionType].
  Map<String, SubstitutionType>? substitutionTypeMap;

  LspbClient({
    Map<String, LspbSubstitutionProperty>? substitutionHeaderMap,
    this.substitutionTypeMap,
  }) {
    this.substitutionHeaderMap =
        substitutionHeaderMap ?? LspbSubstitutionProperty.defaultMap;

    client = ChopperClient(
      baseUrl: Uri.https('start.lspb.de'),
      converter: const JsonSerializableConverter({
        lspb.NewsEntry: lspb.NewsEntry.fromJson,
        InboxResponse: InboxResponse.fromJson,
        UserResponse: UserResponse.fromJson,
        SchedulesResponse: SchedulesResponse.fromJson,
        LoginCheckResponse: LoginCheckResponse.fromJson,
      }),
      interceptors: [
        _browserImpersonationInterceptor,
        phpSessionInterceptor = PhpSessionInterceptor(),
      ],
      services: [LspbService.create()],
    );
    service = client.getService<LspbService>();
  }

  static Request _browserImpersonationInterceptor(Request request) {
    return request.copyWith(
      headers: {
        ...request.headers,
        'X-Requested-With': 'XMLHttpRequest',
        'Origin': 'https://start.lspb.de',
        'Referer': 'https://start.lspb.de/',
        'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; rv:106.0) Gecko/20100101 Firefox/106.0'
      },
    );
  }

  @override
  Future<List<Substitution>> getSubstitutions() async {
    final schedulesResponse = await service.getSchedules();

    if (!schedulesResponse.isSuccessful) {
      throw Exception('Failed to get substitutions');
    }

    final schedule = schedulesResponse.body!;
    final headers = schedule.headers.map((e) => e.trim()).toList();

    final typeMap = substitutionTypeMap ?? _defaultSubstitutionTypeMap;

    return schedule.entries.map((e) {
      final items = e.items;

      final map = {
        for (final key in LspbSubstitutionProperty.values)
          key: items[headers.indexWhere(
            (e) => substitutionHeaderMap[e] == key,
          )],
      };
      final dateFormat = DateFormat('d.M.');
      final timeFormat = DateFormat('H:mm');
      final date = dateFormat
          .parse(map[LspbSubstitutionProperty.date]!)
          .copyWith(year: DateTime.now().year);
      final time = map[LspbSubstitutionProperty.time]!
          .split('-')
          .map(timeFormat.parse)
          .map(
            (e) => e.copyWith(
              month: date.month,
              year: date.year,
              day: date.day,
            ),
          );

      final typeField = map[LspbSubstitutionProperty.type];
      final substitutionType = typeMap[typeField]!;
      return Substitution(
        type: substitutionType,
        startTime: time.first,
        endTime: time.last,
        room: map[LspbSubstitutionProperty.room],
        absentTeacher: map[LspbSubstitutionProperty.absentTeacher],
        substituteTeacher: map[LspbSubstitutionProperty.substituteTeacher],
      );
    }).toList();
  }

  @override
  Future<List<NewsEntry>> getNews() async {
    final response = await service.getNewsEntries();
    return response.body!
        .map((e) => NewsEntry(e.permalink, e.title, e.content))
        .toList();
  }

  @override
  Future<String> getIdentity() async {
    final response = await service.getUser();
    return response.body!.displayName!;
  }
}

enum LspbSubstitutionProperty {
  date('Datum'),
  time('Zeit'),
  classes('Klasse(n)'),
  absentTeacher('Abwesende/r Lehrer/in'),
  affectedSubject('Betroffenes Fach'),
  substituteTeacher('Vertreter/in'),
  substituteSubject('Vertretungs- fach'),
  room('in Raum'),
  type('Art'),
  comment('Informationen zur Vertretung');

  final String? defaultHeader;

  const LspbSubstitutionProperty([this.defaultHeader]);

  static Map<String, LspbSubstitutionProperty> get defaultMap {
    return {
      for (final value in values)
        if (value.defaultHeader != null) value.defaultHeader!: value,
    };
  }
}
