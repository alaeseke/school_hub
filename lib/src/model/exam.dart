class Exam {
  final String id;
  final String title;
  final List<String> topics;
  final DateTime date;
  final String room;
  final List<String> supervisingTeachers;

  const Exam(
    this.id,
    this.title,
    this.topics,
    this.date,
    this.room,
    this.supervisingTeachers,
  );
}
