class Event {
  final String title;
  final DateTime startTime;
  final DateTime? endTime;
  final EventType? type;

  const Event({
    required this.title,
    required this.startTime,
    this.endTime,
    this.type,
  });
}

enum EventType {
  parentTeacherConference,
  festival,
  extracurricular,
  holidays,
}
