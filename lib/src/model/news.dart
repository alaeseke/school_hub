class NewsEntry {
  final Uri url;
  final String title;
  final String description;

  NewsEntry(this.url, this.title, this.description);
}
