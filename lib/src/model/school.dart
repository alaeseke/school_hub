class School {
  final String name;
  final String slug;
  final Location? location;

  School({required this.name, required this.slug, this.location});
}

class Location {
  final String longitude;
  final String latitude;

  Location(this.longitude, this.latitude);
}
