import 'package:meta/meta.dart';

@immutable
class Substitution {
  final SubstitutionType type;
  final String? room;
  final String? absentTeacher;
  final String? substituteTeacher;
  final DateTime? startTime;
  final DateTime? endTime;

  const Substitution({
    required this.type,
    this.room,
    this.absentTeacher,
    this.substituteTeacher,
    this.startTime,
    this.endTime,
  });
}

enum SubstitutionType {
  /// The lesson was dropped.
  dropped,

  /// The lesson is substituted
  substitution,

  /// The lesson is substituted without a teacher.
  substitutionWithoutTeacher,
  roomChange,
}
