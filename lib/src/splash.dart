import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:school_hub/src/app.dart';
import 'package:school_hub/src/localization/app_localizations.dart';
import 'package:school_hub/src/logo_icon.dart';

class SplashView extends StatelessWidget {
  const SplashView();

  @override
  Widget build(BuildContext context) {
    final [theme, darkTheme] = MyApp.getThemes((light: null, dark: null));
    return MaterialApp(
      home: Center(
        child: IconTheme(
          data: IconThemeData(
            color: Colors.green.shade900,
            size: 96,
          ),
          child: const LogoIcon(),
        ),
      ),
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''), // English, no country code
        Locale('de', ''), // English, no country code
      ],
      onGenerateTitle: (context) => AppLocalizations.of(context)!.appTitle,
      theme: theme,
      darkTheme: darkTheme,
    );
  }
}
