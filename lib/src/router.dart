import 'dart:math';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:school_hub/src/main/main_view.dart';
import 'package:school_hub/src/model/exam.dart';
import 'package:school_hub/src/model/school.dart';
import 'package:school_hub/src/onboarding/choose_school_page.dart';
import 'package:school_hub/src/onboarding/onboarding_view.dart';
import 'package:school_hub/src/settings/settings_view.dart';

final router = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) {
        return const MainView();
      },
    ),
    GoRoute(
      path: '/calendar/add',
      name: 'add_calendar_event',
      builder: (context, state) {
        return const AddEventPage();
      },
    ),
    GoRoute(
      path: '/settings',
      name: 'settings',
      builder: (context, state) {
        return const SettingsView();
      },
    ),
    ShellRoute(
      routes: [
        GoRoute(
          path: '/setup',
          builder: (context, state) {
            return const OnboardingScreen();
          },
          routes: [
            GoRoute(
              path: 'school',
              builder: (context, state) {
                return ChooseSchoolPage(
                  onSchoolSelected: (School value) {
                    context.pop(value);
                  },
                );
              },
            ),
            GoRoute(
              path: 'services',
              builder: (context, state) {
                return const Text('Services');
              },
            ),
          ],
        ),
      ],
    ),
  ],
);

class AddEventPage extends StatefulWidget {
  const AddEventPage({
    super.key,
  });

  @override
  State<AddEventPage> createState() => _AddEventPageState();
}

class _AddEventPageState extends State<AddEventPage> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _roomController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog.fullscreen(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(Icons.close),
            onPressed: () => Navigator.of(context).maybePop(),
          ),
          title: const Text('Add Calendar Event'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).maybePop(
                  Exam(
                    Random().nextInt(100000).toString(),
                    _titleController.text,
                    [],
                    DateTime.now(),
                    _roomController.text,
                    [],
                  ),
                );
              },
              child: const Text('Save'),
            ),
          ],
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              DropdownButtonFormField(
                decoration: const InputDecoration(
                  labelText: 'Event Kind',
                  border: OutlineInputBorder(),
                ),
                items: const [
                  DropdownMenuItem(
                    value: 'exam',
                    child: Text('Exam'),
                  ),
                ],
                onChanged: (String? value) {},
              ),
              const SizedBox(height: 16),
              TextField(
                decoration: const InputDecoration(
                  labelText: 'Title',
                  border: OutlineInputBorder(),
                ),
                controller: _titleController,
                autofocus: true,
              ),
              const SizedBox(height: 16),
              FormField(
                builder: (state) {
                  return Row(
                    children: [
                      Expanded(
                        child: TextField(
                          decoration: const InputDecoration(
                            labelText: 'Start Date',
                            border: OutlineInputBorder(),
                          ),
                          controller: TextEditingController(
                            text: DateTime.now().toIso8601String(),
                          ),
                          readOnly: true,
                          onTap: () {
                            showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now(),
                              lastDate:
                                  DateTime.now().add(const Duration(days: 365)),
                            ).then((value) {
                              if (value != null) {}
                            });
                          },
                        ),
                      ),
                    ],
                  );
                },
              ),
              const SizedBox(height: 16),
              TextField(
                decoration: const InputDecoration(
                  labelText: 'Room',
                  border: OutlineInputBorder(),
                ),
                controller: _roomController,
                autofocus: true,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
