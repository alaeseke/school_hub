import 'dart:async';

import 'package:flutter/material.dart';
import 'package:school_hub/src/icons.dart';
import 'package:school_hub/src/localization/app_localizations.dart';

class LspbLoginDialog extends StatefulWidget {
  final Future<void> Function(String username, String password) onLogin;

  const LspbLoginDialog({super.key, required this.onLogin});

  @override
  State<LspbLoginDialog> createState() => _LspbLoginDialogState();
}

class _LspbLoginDialogState extends State<LspbLoginDialog> {
  Future? _future;
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    return Dialog.fullscreen(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Sign in to Lernstatt'),
          centerTitle: true,
        ),
        body: FutureBuilder(
          future: _future,
          builder: (context, snapshot) {
            final isSigningIn =
                snapshot.connectionState == ConnectionState.waiting;
            const inputBorder = OutlineInputBorder();
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(
                    height: 200,
                    child: ColoredBox(
                      color: Theme.of(context).colorScheme.surfaceVariant,
                      child: Center(
                        child: Icon(
                          SchoolHubIcons.lspb,
                          size: 96,
                          color: Theme.of(context).colorScheme.onSurfaceVariant,
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(maxWidth: 600),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TextField(
                              controller: _usernameController,
                              decoration: InputDecoration(
                                hintText: 'mmuster1',
                                labelText: localizations.usernameHint,
                                border: inputBorder,
                              ),
                              autofocus: true,
                              enabled: !isSigningIn,
                              textInputAction: TextInputAction.next,
                            ),
                            const SizedBox(height: 16),
                            TextField(
                              controller: _passwordController,
                              decoration: InputDecoration(
                                labelText: localizations.passwordHint,
                                border: inputBorder,
                              ),
                              obscureText: true,
                              enabled: !isSigningIn,
                              textInputAction: TextInputAction.done,
                              onEditingComplete: () {
                                if (isSigningIn) return;
                                _onSignIn();
                              },
                            ),
                            if (snapshot.hasError) ...[
                              const SizedBox(height: 16),
                              Text(
                                snapshot.error.toString(),
                              ),
                            ],
                            const SizedBox(height: 16),
                            Align(
                              alignment: Alignment.topRight,
                              child: FilledButton(
                                onPressed:
                                    isSigningIn ? null : () => _onSignIn(),
                                child: Text(localizations.signInButtonLabel),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  void _onSignIn() {
    return setState(
      () {
        _future = widget.onLogin(
          _usernameController.text,
          _passwordController.text,
        );
      },
    );
  }
}
