import 'package:dynamic_color/dynamic_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:school_hub/color_schemes.g.dart';
import 'package:school_hub/src/localization/app_localizations.dart';
import 'package:school_hub/src/router.dart';
import 'package:school_hub/src/settings/settings_controller.dart';

/// The Widget that configures your application.
class MyApp extends StatelessWidget {
  const MyApp({
    super.key,
    required this.settingsController,
  });

  final SettingsController settingsController;

  @override
  Widget build(BuildContext context) {
    return DynamicColorBuilder(
      builder: (dynamicLight, dynamicDark) {
        return AnimatedBuilder(
          animation: settingsController,
          builder: (BuildContext context, Widget? child) {
            final [theme, darkTheme] =
                getThemes((light: dynamicLight, dark: dynamicDark));
            return MaterialApp.router(
              restorationScopeId: 'app',
              localizationsDelegates: const [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: const [
                Locale('en', ''), // English, no country code
                Locale('de', ''), // English, no country code
              ],
              locale: Locale('de'),
              onGenerateTitle: (context) =>
                  AppLocalizations.of(context)!.appTitle,
              theme: theme,
              darkTheme: darkTheme,
              themeMode: settingsController.themeMode,
              routerConfig: router,
            );
          },
        );
      },
    );
  }

  static List<ThemeData> getThemes(
    ({ColorScheme? light, ColorScheme? dark}) dynamicColorSchemes,
  ) {
    final themes = [
      ThemeData.from(
        useMaterial3: true,
        colorScheme: dynamicColorSchemes.light ?? lightColorScheme,
      ),
      ThemeData.from(
        useMaterial3: true,
        colorScheme: dynamicColorSchemes.dark ?? lightColorScheme,
      ),
    ];

    final modifiedThemes = themes.map(
      (e) => e.copyWith(
        cardTheme: CardTheme(
          elevation: 2,
          surfaceTintColor: e.colorScheme.surfaceTint,
          shadowColor: Colors.transparent,
          color: e.colorScheme.surface,
          margin: EdgeInsets.zero,
        ),
      ),
    );

    return modifiedThemes.toList(growable: false);
  }
}
