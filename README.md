# School Hub

[![style: lint](https://img.shields.io/badge/style-lint-4BC0F5.svg)](https://pub.dev/packages/lint)

A digital home for everything school related, made for the [Youth Hacking 4 Freedom 2023](https://fsfe.org/activities/yh4f/).

## Primary inspiration

Current solutions are proprietary, custom, and/or often worse than what everyone else could make.

This project tries to build a beautiful and useful app to help the average student use data more quickly.

## Features

- Integration with Lernstatt-Paderborn (LSPB)
- Calendar
- Substitutions
- News feed

## Planned features

- [Onboarding](lib/src/onboarding/onboarding_view.dart)
    - [School list](assets/schools.json)
- [Calendar editing](lib/src/router.dart)
- Push notifications
- Backend for allowing students to collaborate on data
    - Exam list
    - Lesson notes
    - Homework
    - Message board
    - Class/course list

## License

This project is licensed under the AGPL v3.0. This project contains a modified version of `flutter_calendar_view`, licensed under the MIT license.